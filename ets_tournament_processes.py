

import logging
import ets_utils
import crest_tournament
import ets_tournaments

from ets_models import TournamentInfoModel, TournamentModel, TournamentTeamModel

from google.appengine.ext import deferred
from google.appengine.ext import ndb


def process_update_basic_tournament(tourney_id):
    """
    @param tournament_url : A CREST tournament URL string
    """

    tournament_url = ets_tournaments.get_single_tournament(tourney_id).url

    ets_utils.is_valid_tournament_url(tournament_url)

    crest_results = ets_utils.get_crest_url(tournament_url, to_cache=True)

    if crest_results is None:
        logging.error('The selected tournament_url returned None %s' % tournament_url)
        return

    if len(crest_results['entries']) <= 0:
        raise Exception('The selected tournament does not have any teams in it yet. %s' % tournament_url)

    parent_key      = ndb.Key(TournamentModel, tourney_id)
    tournament_info = TournamentInfoModel.get_by_id(tourney_id, parent_key)

    if tournament_info is None:
        logging.info('Tournament_info does not exist, creating new tournament_info for tourney_id %s' % tourney_id)
        tournament_info = TournamentInfoModel(parent=parent_key, id=tourney_id)

    tournament                  = crest_tournament.Tournament(crest_results)
    tournament_info.teams_total = tournament.get_team_count()
    tournament_info.series_url  = tournament.get_series_url()

    team_urls = []
    for team_url in tournament.get_team_urls():
        team_urls.append(team_url)

    tournament_info.team_urls = team_urls

    ancestor_key = ndb.Key(TournamentModel, tourney_id, TournamentInfoModel, tourney_id)
    teams = TournamentTeamModel.query(ancestor=ancestor_key).fetch()

    damage_done    = 0
    isk_killed     = 0
    ships_killed   = 0
    pilots_fielded = 0

    for team in teams:
        damage_done    += team.damage_done
        isk_killed     += team.isk_killed
        ships_killed   += team.ships_killed
        pilots_fielded += team.pilots_fielded_count

    tournament_info.damage_done_total    = damage_done
    tournament_info.isk_destroyed_total  = isk_killed
    tournament_info.ships_killed_total   = ships_killed
    tournament_info.pilots_fielded_total = pilots_fielded

    tournament_info.put()


def process_update_team(tourney_id, team_url):
    team_id         = team_url
    parent_key      = ndb.Key(TournamentModel, tourney_id, TournamentInfoModel, tourney_id)
    tournament_team = TournamentTeamModel.get_by_id(team_id, parent=parent_key)

    if tournament_team is None:
        logging.info('Tournament team does not exists, creating one with the id of %s' % team_id)
        tournament_team = TournamentTeamModel(parent=parent_key, id=team_id)

    crest_results = ets_utils.get_crest_url(team_url, to_cache=True, cache_time=600)
    team          = crest_tournament.Team(crest_results)

    tournament_team.name                 = team.get_name()
    tournament_team.ships_killed         = team.get_ships_killed()
    tournament_team.isk_killed           = team.get_isk_killed()
    tournament_team.damage_done          = 0
    tournament_team.captain_id           = team.get_captain_id()
    tournament_team.captain_name         = team.get_captain_name()
    tournament_team.pilots_fielded_count = team.get_pilots_fielded_count()

    tournament_team.put()


def process_update_series(tourney_id):
    parent_key      = ndb.Key(TournamentModel, tourney_id)
    tournament_info = TournamentInfoModel.get_by_id(tourney_id, parent_key)

    if tournament_info is None:
        logging.error('Tried to process a series but tournament_info was None for tourney_id %s' % tourney_id)
        return

    crest_results     = ets_utils.get_crest_url(tournament_info.series_url, to_cache=True, cache_time=600)
    series_collection = crest_tournament.SeriesCollection(crest_results)

    # Update the tournament info with information from the series
    tournament_info.series_total     = series_collection.series_total
    tournament_info.series_completed = series_collection.series_completed
    tournament_info.series_wins_red  = series_collection.red_wins
    tournament_info.series_wins_blue = series_collection.blue_wins

    tournament_info.put()