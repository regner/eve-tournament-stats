#!/usr/bin/env python

import ets_utils
import ets_tournaments
import ets_tournament_info

from flask import Flask, render_template, flash, redirect

app = Flask(__name__.split('.')[0])
app.secret_key = ets_utils.secret_key


@app.route('/')
def home():
    """
    Home page for the EVE Online Tournament Stats web site
    """

    tournaments     = ets_tournaments.get_all_tournaments()
    template_values = {'base_info'   : ets_utils.get_basic_template_values(),
                       'tournaments' : []}

    for tourney in tournaments:
        tournament_values = ets_tournaments.format_tournament_as_dict(tourney)

        template_values['tournaments'].append(tournament_values)

    return render_template('home.html', template_values=template_values)


@app.route('/tournament/<tourney_id>/')
def tournament(tourney_id=None):

    if ets_tournaments.tournament_exists(tourney_id) is not True:
        flash(ets_utils.error_messages['tournament_not_exists'], 'alert-warning')
        return redirect('/')

    tournament_info = ets_tournament_info.get_tournament_info(tourney_id)

    template_values = {'base_info'       : ets_utils.get_basic_template_values(),
                       'nav_tournament'  : ets_utils.get_tournament_navigation(),
                       'tournament_info' : ets_tournament_info.format_tournament_info_as_dict(tournament_info)}

    return render_template('tournament.html', template_values=template_values)