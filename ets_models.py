"""
Google App Engine models for the EVE Tournament Stats web app.
"""

from google.appengine.ext import ndb


class TournamentModel(ndb.Model):
    """
    Model for each unique tournament and it's associated basic information.
    """

    name     = ndb.StringProperty()
    url      = ndb.StringProperty()
    desc     = ndb.BlobProperty()
    order    = ndb.IntegerProperty()
    running  = ndb.BooleanProperty()


class TournamentInfoModel(ndb.Model):
    """
    Model for each unique tournament and their expanded overall information.
    This model should have a TornamentModel parent.
    """

    series_total         = ndb.IntegerProperty()
    series_completed     = ndb.IntegerProperty()
    series_wins_red      = ndb.IntegerProperty()
    series_wins_blue     = ndb.IntegerProperty()
    ships_killed_total   = ndb.IntegerProperty()
    pilots_fielded_total = ndb.IntegerProperty()
    teams_total          = ndb.IntegerProperty()
    ships_fielded        = ndb.IntegerProperty()
    isk_destroyed_total  = ndb.FloatProperty()
    damage_done_total    = ndb.FloatProperty()
    series_url           = ndb.StringProperty()
    team_urls            = ndb.StringProperty(repeated=True)


class TournamentTeamModel(ndb.Model):
    """
    Model for each team in a tournament containing team specific statistics and information.
    This model should have a TournamentInforModel parent.
    """

    name                 = ndb.StringProperty()
    ships_killed         = ndb.IntegerProperty()
    isk_killed           = ndb.FloatProperty()
    damage_done          = ndb.FloatProperty()
    captain_id           = ndb.IntegerProperty()
    captain_name         = ndb.StringProperty()
    pilots_fielded_count = ndb.IntegerProperty()