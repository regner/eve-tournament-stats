
import logging
import requests
import ets_tournaments

from google.appengine.api import memcache

secret_key     = 'aishd98uiasy6d897uayshd7o8t78uaitsd786atsdo67a8srdfyuashr78135gti'
error_messages = {'tournament_exists'     : 'Tournament with that short already exists.',
                  'tournament_not_exists' : 'The selected tournament does not seem to exist.'}


def get_basic_template_values(quantity=3):
    """
    Returns basic values that should be sent as part of the template values for any
    template that extends base.html
    """

    tournaments      = ets_tournaments.get_all_tournaments()[:quantity]
    tournaments_temp = []

    for tourney in tournaments:
        tournaments_temp.append(ets_tournaments.format_tournament_as_dict(tourney))

    base_info = {'nav_top_tournaments' : tournaments_temp}

    return base_info


def get_tournament_navigation():
    """
    Returns template values used for any page that has the tournament navigation header
    or side navigation.
    """

    nav_tournament = [('overview', 'Overview'),
                      ('teams',    'Teams'),
                      ('series',   'Series'),
                      ('brackets', 'Brackets')]

    return nav_tournament


def get_crest_url(url, headers={}, to_cache=False, cache_time=None):
    """
    Get a specified CREST URL ensuring we first check NDB for a cached version
    """

    results = memcache.get(url)

    if results is None:
        headers['User-Agent'] = 'EVE Tournament Stats by Regner Blok-Andersen'

        results = requests.get(url, headers=headers)

        if results.status_code != 200:
            logging.error('Failed to fetch CREST URL %s' % url)
            return None

        results = results.json()

        if to_cache is True:
            if cache_time is not None:
                memcache.add(url, results, cache_time)

            else:
                memcache.add(url, results)

    if results is None:
        raise Exception('CREST results is None... WTF!? %s' % url)

    return results


def is_valid_tournament_url(tournament_url):
    if tournament_url.split('/')[-3] != 'tournaments':
        raise Exception('It appears an invalid tournament URL was supplied.')