"""
Contains the tournaments class for the EVE Tournament Stats web application.
"""

import ets_utils

from ets_models import TournamentModel


def get_all_tournaments():
    """
    @rtype  :  list
    @return : Returns a list of all tournaments and their basic information.
    """

    tournaments     = TournamentModel
    all_tournaments = tournaments.query().order(-tournaments.order).fetch()

    return all_tournaments


def get_running_tournaments():
    tournaments = TournamentModel
    running_tournaments = tournaments.query(tournaments.running == True).fetch()

    return running_tournaments


def get_single_tournament(tourney_id):
    """
    @type  tourney_id : str
    @param tourney_id : string of the short name for the desired tournament
    @rtype            : TournamentModel
    @return           : Returns a single tournament
    """

    return TournamentModel.get_by_id(tourney_id)


def add_new_tournament(tournament_properties):
    """
    tournament_properties must be a dictionary with the following keys:
    - name
    - tourney_id
    - url
    - order
    - desc
    """

    if tournament_exists(tournament_properties['tourney_id']) is True:
        raise Exception(ets_utils.error_messages['tournament_exists'])

    new_tournament = TournamentModel(id=tournament_properties['tourney_id'])
    new_tournament.name       = tournament_properties['name']
    new_tournament.url        = tournament_properties['url']
    new_tournament.order      = tournament_properties['order']
    new_tournament.desc       = tournament_properties['desc']
    new_tournament.running    = tournament_properties['running']
    new_tournament.put()


def update_existing_tournament(tournament_properties):
    tournament         = get_single_tournament(tournament_properties['tourney_id'])
    tournament.name    = tournament_properties['name']
    tournament.url     = tournament_properties['url']
    tournament.order   = tournament_properties['order']
    tournament.desc    = tournament_properties['desc']
    tournament.running = tournament_properties['running']
    tournament.put()


def format_tournament_as_dict(tournament):
    """
    Takes a tournament DB model and returns it as a key value dictionary.

    @rtype : dict
    """

    return {'id'      : tournament.key.id(),
            'name'    : tournament.name,
            'url'     : tournament.url,
            'order'   : tournament.order,
            'desc'    : tournament.desc,
            'running' : tournament.running}


def tournament_exists(tourney_id):
    """
    Check if a specified tournament exists or not.

    @rtype : bool
    """
    tournament_check = TournamentModel.get_by_id(tourney_id)

    if tournament_check is not None:
        return True

    return False
