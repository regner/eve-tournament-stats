

from google.appengine.ext import ndb

from ets_models import TournamentInfoModel, TournamentModel


def get_tournament_info(tourney_id):
    parent_key = ndb.Key(TournamentModel, tourney_id)
    return TournamentInfoModel.get_by_id(tourney_id, parent_key)


def format_tournament_info_as_dict(tournament_info):
    return {'id'               : tournament_info.key.id(),
            'series_total'     : tournament_info.series_total,
            'series_completed' : tournament_info.series_completed,
            'series_wins_red'  : tournament_info.series_wins_red,
            'series_wins_blue' : tournament_info.series_wins_blue,
            'teams_total'      : tournament_info.teams_total,
            'ships_killed'     : '{:,d}'.format(tournament_info.ships_killed_total),
            'isk_killed'       : '{:,.0f}'.format(tournament_info.isk_destroyed_total),
            'damage_done'      : '{:,.0f}'.format(tournament_info.damage_done_total),
            'pilots_fielded'   : '{:,d}'.format(tournament_info.pilots_fielded_total)}