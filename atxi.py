import cgi
import time
import logging
import webapp2
import operator
import requests

from google.appengine.ext import ndb
from google.appengine.api import memcache


def getUrl(url, headers=None):
    i = 0

    while i < 10:

        r = requests.get(url, headers=headers)

        if r.status_code == 503:
            logging.info('Sleeping 1 second due to 503. Current URL: %s' % url)
            time.sleep(1)

            i += 1

            continue

        else:
            return r

    raise Exception('Unable to fetch URL: %s' % url)




class TournamentModel(ndb.Model):
    tournament = ndb.JsonProperty()

    @classmethod
    def queryTournament(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).fetch(1)


class TournamentCron(webapp2.RequestHandler):
    def get(self):
        tournamentUrl           = 'http://public-crest.eveonline.com/tournaments/1/'
        tournamentData          = TournamentData(tournamentUrl)

        try:
            ancestor_key = ndb.Key('TournamentModel', 'ATXI')
            tournamentDB = TournamentModel.queryTournament(ancestor_key)[0]

        except:
            tournamentDB = None

        if tournamentDB is None:
            tournamentDB = TournamentModel(id='ATXI')

        tournamentDB.tournament = tournamentData.tournament
        tournamentDB.put()


class TournamentData():
    def __init__(self, url):
        self.tournament                        = getUrl(url).json()
        self.tournament['totalIskDestroyed']   = 0
        self.tournament['totalShipsDestroyed'] = 0
        self.tournament['redTeamWins']         = 0
        self.tournament['blueTeamWins']        = 0
        self.tournament['totalDamage']         = 0
        self.tournament['completedMatches']    = {}
        self.tournament['teams']               = {}
        self.tournament['ships']               = {}

        self.__processSeries()
        self.__processTeams()
        self.__processMatches()

    def __processSeries(self):
        self.tournament['series']                   = getUrl(self.tournament['series']['href']).json()
        self.tournament['series']['completedCount'] = 0

        for entry in self.tournament['series']['items']:
            if entry['winner']['isDecided'] == True:
                self.tournament['series']['completedCount'] += 1

                if entry['redTeam']['team']['href'] == entry['winner']['team']['href']:
                    self.tournament['redTeamWins'] += 1

                else:
                    self.tournament['blueTeamWins'] += 1

            matches = getUrl(entry['matches']['href']).json()
            for match in matches['items']:
                if match['finalized'] == True:
                    match['self'] = {'href': '%s%s/' % (entry['matches']['href'], matches['items'].index(match))}
                    self.tournament['completedMatches'][match['self']['href']] = match

    def __processTeams(self):
        for entry in self.tournament['entries']:
            team = getUrl(entry['href']).json()

            self.tournament['teams'][entry['href']] = team

            self.tournament['teams'][entry['href']]['matchesPlayed']  = 0
            self.tournament['teams'][entry['href']]['damageDone']     = 0
            self.tournament['teams'][entry['href']]['damageReceived'] = 0
            self.tournament['teams'][entry['href']]['iskLost']        = 0
            self.tournament['teams'][entry['href']]['shipsLost']      = 0

            self.tournament['totalIskDestroyed']   += team['iskKilled']
            self.tournament['totalShipsDestroyed'] += team['shipsKilled']

    def __processMatches(self):
        for match in self.tournament['completedMatches']:
            self.tournament['teams'][self.tournament['completedMatches'][match]['redTeam']['href']]['matchesPlayed']  += 1
            self.tournament['teams'][self.tournament['completedMatches'][match]['blueTeam']['href']]['matchesPlayed'] += 1

            matchStats = getUrl(self.tournament['completedMatches'][match]['stats']['pilots']['href']).json()

            for team in ['redTeam', 'blueTeam']:
                for ban in self.tournament['completedMatches'][match]['bans'][team][0]['typeBans']:
                    self.__addShip(ban['name'])

                    self.tournament['ships'][ban['name']]['banned'] += 1

            for pilot in matchStats['items']:
                self.tournament['teams'][pilot['team']['href']]['damageDone']     += pilot['damageDone']
                self.tournament['teams'][pilot['team']['href']]['damageReceived'] += pilot['damageReceived']
                self.tournament['totalDamage']                                    += pilot['damageDone']

                # Track fielded, killer, and victim ship stats
                self.__addShip(pilot['shipType']['name'])

                self.tournament['ships'][pilot['shipType']['name']]['fielded'] += 1

                if pilot['isDead'] == True:
                    self.tournament['ships'][pilot['shipType']['name']]['victim'] += 1
                    self.tournament['teams'][pilot['team']['href']]['shipsLost']  += 1
                    self.tournament['teams'][pilot['team']['href']]['iskLost']    += pilot['shipValue']

                    for pilotKiller in matchStats['items']:
                        if pilotKiller['pilot']['href'] == pilot['killer']['href']:
                            self.__addShip(pilotKiller['shipType']['name'])

                            self.tournament['ships'][pilotKiller['shipType']['name']]['killer'] += 1

    def __addShip(self, name):
        if name not in self.tournament['ships']:
            self.tournament['ships'][name] = {'fielded': 0,
                                              'killer' : 0,
                                              'victim' : 0,
                                              'banned' : 0,
                                              }


class Tournament():
    def __init__(self, data):
        self.tournament = data

    def getTotalIskDestroyed(self):
        return '{:,d}'.format(self.tournament['totalIskDestroyed'])

    def getTotalShipsKilled(self):
        return '{:,d}'.format(self.tournament['totalShipsDestroyed'])

    def getTeamIskKilled(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['iskKilled']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamIskLost(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['iskLost']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamIskKilledAndLost(self):
        tempL   = []
        iskLost = 0

        for teamName, iskKilled in self.getTeamIskKilled():
            for team in self.tournament['teams']:
                if self.tournament['teams'][team]['name'] == teamName:
                    iskLost = self.tournament['teams'][team]['iskLost']

            tempL.append((teamName, iskKilled, iskLost))

        return tempL

    def getTeamShipsKilled(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['shipsKilled']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamShipsLost(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['shipsLost']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamShipsKilledAndLost(self):
        tempL   = []
        shipsLost = 0

        for teamName, shipsKilled in self.getTeamShipsKilled():
            for team in self.tournament['teams']:
                if self.tournament['teams'][team]['name'] == teamName:
                    shipsLost = self.tournament['teams'][team]['shipsLost']

            tempL.append((teamName, shipsKilled, shipsLost))

        return tempL

    def getRedTeamWins(self):
        return self.tournament['redTeamWins']

    def getBlueTeamWins(self):
        return self.tournament['blueTeamWins']

    def getSeriesTotalCount(self):
        return self.tournament['series']['totalCount']

    def getSeriesTogoCount(self):
        return self.tournament['series']['totalCount'] - self.tournament['series']['completedCount']

    def getSeriesCompleted(self):
        return self.tournament['series']['completedCount']

    def getTotalPilotCount(self):
        count = 0

        for team in self.tournament['teams']:
            count += len(self.tournament['teams'][team]['pilots'])

        return count

    def getTeamPilotCounts(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = len(self.tournament['teams'][team]['pilots'])

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTotalMatchesPlayed(self):
        return len(self.tournament['completedMatches'])

    def getTeamMatchesPlayed(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['matchesPlayed']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamSizeWinRate(self):
        returnD = {}

        # TODO: Move the gathering getTeamSizeWinRate stats from this to the tournament update class
        for match in self.tournament['completedMatches']:
            pilotCount = 0
            winner     = match['winner']['href']
            matchStats = getUrl(self.tournament['completedMatches'][match]['stats']['pilots']['href'])

            for pilot in matchStats['items']:
                if pilot['team']['href'] == winner:
                    pilotCount += 1

            if pilotCount in returnD:
                returnD[pilotCount] += 1

            else:
                returnD[pilotCount] = 1

        return returnD

    def getTotalDamage(self):
        return '{:,f}'.format(self.tournament['totalDamage'])

    def getTeamDamageDone(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['damageDone']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamDamageReceived(self):
        tempD = {}

        for team in self.tournament['teams']:
            tempD[self.tournament['teams'][team]['name']] = self.tournament['teams'][team]['damageReceived']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTeamDamageDoneAndReceived(self):
        tempL   = []

        for teamName, damageDone in self.getTeamDamageDone():
            for team in self.tournament['teams']:
                if self.tournament['teams'][team]['name'] == teamName:
                    tempL.append((teamName, damageDone, self.tournament['teams'][team]['damageReceived']))
                    break

        return tempL

    def getShipsFielded(self):
        tempD = {}

        for ship in self.tournament['ships']:
            tempD[ship] = self.tournament['ships'][ship]['fielded']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getShipsKiller(self):
        tempD = {}

        for ship in self.tournament['ships']:
            tempD[ship] = self.tournament['ships'][ship]['killer']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getShipsVictim(self):
        tempD = {}

        for ship in self.tournament['ships']:
            tempD[ship] = self.tournament['ships'][ship]['victim']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getShipsBanned(self):
        tempD = {}

        for ship in self.tournament['ships']:
            tempD[ship] = self.tournament['ships'][ship]['banned']

        return sorted(tempD.iteritems(), key=operator.itemgetter(1))

    def getTotalShipsFielded(self):
        tempI = 0

        for ship in self.tournament['ships']:
            tempI += self.tournament['ships'][ship]['fielded']

        return tempI

    def getCombinedShipStats(self):
        tempL = []

        for ship, fieldedCount in self.getShipsFielded():
            tempL.append((ship,
                          fieldedCount,
                          self.tournament['ships'][ship]['killer'],
                          self.tournament['ships'][ship]['victim'],
                          self.tournament['ships'][ship]['banned']))

        return tempL

    def getMatchesWithRealTimeStats(self):
        matchesD = {}

        for match in self.tournament['completedMatches']:
            if 'firstReplayFrame' in self.tournament['completedMatches'][match]:
                try:
                    ancestor_key = ndb.Key('MatchRealTimeStatsModel', match)
                    matchFrameDB = MatchRealTimeStatsModel.queryMatch(ancestor_key)[0]

                except:
                    matchFrameDB = None

                if matchFrameDB is not None:
                    pilotStats = self.tournament['completedMatches'][match]['stats']['pilots']['href']
                    seriesNum  = int(pilotStats.split('/')[-5])
                    matchNum   = int(pilotStats.split('/')[-3]) + 1

                    matchesD['%03d-%s' % (seriesNum, matchNum)] = (('%03d' % seriesNum,
                                                                    matchNum,
                                                                    self.tournament['completedMatches'][match]['redTeam']['teamName'],
                                                                    self.tournament['completedMatches'][match]['blueTeam']['teamName'],
                                                                    match))

        matchesL = sorted(matchesD.iteritems(), key=operator.itemgetter(1))
        matchesL2 = []

        for x in matchesL:
            matchesL2.append(x[1])

        return matchesL2

    def convertMatchNumToUrl(self, matchNum):
        series, match = matchNum.split('-')

        series = series.lstrip('0')
        match  = match.lstrip('0')

        matchUrl = self.tournament['completedMatches'].iterkeys().next()

        matchPeices = matchUrl.split('/')

        matchPeices[-4] = series
        matchPeices[-2] = str(int(match) - 1)

        matchUrl = '/'.join(matchPeices)

        return matchUrl


class MatchRealTimeStatsModel(ndb.Model):
    match = ndb.JsonProperty()

    @classmethod
    def queryMatch(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).fetch()


class MatchRealTimeStatsCron(webapp2.RequestHandler):
    def get(self):
        ancestor_key   = ndb.Key('TournamentModel', 'ATXI')
        tournamentData = TournamentModel.queryTournament(ancestor_key)[0]
        tournament     = tournamentData.tournament

        matchUrl = cgi.escape(self.request.get('matchurl'))

        if matchUrl:
            MatchRealTimeStatsData(tournament['completedMatches'][matchUrl])

        else:
            for match in tournament['completedMatches']:
                MatchRealTimeStatsData(tournament['completedMatches'][match])


class MatchRealTimeStatsData():
    def __init__(self, match):
        self.match           = match
        self.match['frames'] = []

        if 'firstReplayFrame' in self.match:
            try:
                ancestor_key = ndb.Key('MatchRealTimeStatsModel', self.match['self']['href'])
                matchFrameDB = MatchRealTimeStatsModel.queryMatch(ancestor_key)[0]

            except:
                frameUrl = self.match['firstReplayFrame']['href']

                while True:
                    blueTeamPoints = 0
                    redTeamPoints  = 0

                    # No time to upgrade to work with v2 of the realtime stats stuff so lock it to v1 for now
                    headers   = {'Accept' : 'application/vnd.ccp.eve.TournamentRealtimeMatchFrame-v1+json'}
                    frameData = getUrl(frameUrl, headers).json()

                    for ship in frameData['redTeamShipData']:
                        if ship['structure'] == 0:
                            blueTeamPoints += ship['points']

                    for ship in frameData['blueTeamShipData']:
                        if ship['structure'] == 0:
                            redTeamPoints += ship['points']

                    selectedStats = {'blueTeamFleetAttributes' : frameData['blueTeamFleetAttributes'],
                                     'redTeamFleetAttributes'  : frameData['redTeamFleetAttributes'],
                                     'redTeamPoints'           : redTeamPoints,
                                     'blueTeamPoints'          : blueTeamPoints}

                    self.match['frames'].append(selectedStats)

                    if 'nextFrame' in frameData:
                        frameUrl = frameData['nextFrame']['href']

                    else:
                        break

                matchFrameDB = MatchRealTimeStatsModel(id=self.match['self']['href'],
                                                       match=self.match)
                matchFrameDB.put()

                memcache.add('MatchStats:%s' % self.match['self']['href'], self.match, 6000)


class MatchRealTimeStats():
    def __init__(self, url):
        if url != "":
            self.match = memcache.get('MatchStats:%s' % url)

            if self.match is None:
                ancestor_key = ndb.Key('MatchRealTimeStatsModel', url)
                self.match = MatchRealTimeStatsModel.queryMatch(ancestor_key)[0].match

        else:
            self.match = None

    def getTotalStats(self):
        if self.match == None:
            logging.warning('No match is set. Be sure to run setMatch first.')
            return

        tempD = {'blueTeam': {'totalEHP'       : [],
                              'maxControl'     : [],
                              'totalReps'      : [],
                              'maxDPS'         : [],
                              'appliedDPS'     : [],
                              'appliedControl' : [],
                              'points'         : [],
                              'teamName'       : self.match['blueTeam']['teamName']},
                 'redTeam' : {'totalEHP'       : [],
                              'maxControl'     : [],
                              'totalReps'      : [],
                              'maxDPS'         : [],
                              'appliedDPS'     : [],
                              'appliedControl' : [],
                              'points'         : [],
                              'teamName'       : self.match['redTeam']['teamName']}}

        for frame in self.match['frames']:
            tempD['blueTeam']['totalEHP'].append(frame['blueTeamFleetAttributes']['totalEHP'] / 800000)
            tempD['blueTeam']['maxControl'].append(frame['blueTeamFleetAttributes']['maxControl'] / 75)
            tempD['blueTeam']['totalReps'].append(frame['blueTeamFleetAttributes']['totalReps'] * 60 / 800000)
            tempD['blueTeam']['maxDPS'].append(frame['blueTeamFleetAttributes']['maxDPS'] / 5000)
            tempD['blueTeam']['appliedDPS'].append(frame['blueTeamFleetAttributes']['appliedDPS'] / 5000)
            tempD['blueTeam']['appliedControl'].append(frame['blueTeamFleetAttributes']['appliedControl'] / 75)
            tempD['blueTeam']['points'].append(frame['blueTeamPoints'] / 100)

            tempD['redTeam']['totalEHP'].append(frame['redTeamFleetAttributes']['totalEHP'] / 800000)
            tempD['redTeam']['maxControl'].append(frame['redTeamFleetAttributes']['maxControl'] / 75)
            tempD['redTeam']['totalReps'].append(frame['redTeamFleetAttributes']['totalReps'] * 60 / 800000)
            tempD['redTeam']['maxDPS'].append(frame['redTeamFleetAttributes']['maxDPS'] / 5000)
            tempD['redTeam']['appliedDPS'].append(frame['redTeamFleetAttributes']['appliedDPS'] / 5000)
            tempD['redTeam']['appliedControl'].append(frame['redTeamFleetAttributes']['appliedControl'] / 75)
            tempD['redTeam']['points'].append(frame['redTeamPoints'] / 100)

        return tempD

app = webapp2.WSGIApplication([('/cron/update',              TournamentCron),
                               ('/cron/updaterealtimestats', MatchRealTimeStatsCron)
                              ], debug=True)