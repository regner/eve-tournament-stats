#!/usr/bin/env python

import ets_utils
import ets_tournaments
import ets_tournament_processes

from google.appengine.ext import deferred

from flask import Flask, render_template, request, redirect, flash

app            = Flask(__name__.split('.')[0])
app.secret_key = ets_utils.secret_key


@app.route('/admin/')
def admin():
    """
    Admin interface for EVE Tournament Stats
    """

    tournaments     = ets_tournaments.get_all_tournaments()
    template_values = {'base_info'   : ets_utils.get_basic_template_values(),
                       'tournaments' : []}

    for tournament in tournaments:
        tournament_values = ets_tournaments.format_tournament_as_dict(tournament)

        template_values['tournaments'].append(tournament_values)

    return render_template('admin.html', template_values=template_values)


@app.route('/admin/add_tournament/', methods=['POST'])
def add_tournament():
    """
    Route for adding an new tournament from POST data.
    """

    if ets_tournaments.tournament_exists(request.form['input_tourney_id']) is True:
        flash(ets_utils.error_messages['tournament_exists'], 'alert-danger')
        return redirect('/admin/')

    tournament_values = {'name'       : request.form['input_name'],
                         'tourney_id' : request.form['input_tourney_id'],
                         'url'        : request.form['input_url'],
                         'order'      : int(request.form['input_order']),
                         'desc'       : str(request.form['input_desc']),
                         'running'    : bool(request.form['input_running'])}

    ets_tournaments.add_new_tournament(tournament_values)

    deferred.defer(ets_tournament_processes.process_update_basic_tournament,
                   tournament_values['tourney_id'])

    return redirect('/admin/')


@app.route('/admin/update_tournament/', methods=['POST'])
def update_tournament():

    tournament_values = {'name'       : request.form['input_name'],
                         'tourney_id' : request.form['input_tourney_id'],
                         'url'        : request.form['input_url'],
                         'order'      : int(request.form['input_order']),
                         'desc'       : str(request.form['input_desc']),
                         'running'    : bool(request.form['input_running'])}

    ets_tournaments.update_existing_tournament(tournament_values)

    deferred.defer(ets_tournament_processes.process_update_basic_tournament,
                   tournament_values['tourney_id'])

    return redirect('/admin/')