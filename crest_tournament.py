

class CrestPage(object):
    def __init__(self, page_results):
        self.page_results = page_results


class Tournament(CrestPage):
    def get_team_urls(self):
        team_urls = []
        for team in self.page_results['entries']:
            team_urls.append(team['teamStats']['href'])

        return team_urls

    def get_team_count(self):
        return len(self.page_results['entries'])

    def get_series_url(self):
        return self.page_results['series']['href']


class SeriesCollection(CrestPage):
    def __init__(self, page_results):
        super(SeriesCollection, self).__init__(page_results)

        self.series           = self._get_series()
        self.series_total     = self.page_results['totalCount']
        self.series_completed = self._get_series_completed()
        self._red_wins        = None
        self._blue_wins       = None

    def _get_series(self):
        series = []

        for item in self.page_results['items']:
            series.append(Series(item))

        return series

    def _get_series_completed(self):
        series_completed = 0

        for series in self.series:
            if series.is_series_decided() is True:
                series_completed += 1

        return series_completed

    def _calculate_red_wins(self):
        red_wins = 0

        for series in self.series:
            if series.is_red_winner() is True:
                red_wins += 1

        return red_wins

    def _calculate_blue_wins(self):
        return self.series_completed - self._red_wins

    @property
    def red_wins(self):
        if self._red_wins is not None:
            return self._red_wins

        self._red_wins = self._calculate_red_wins()

        return self._red_wins

    @red_wins.setter
    def red_wins(self, value):
        self._red_wins = value

    @property
    def blue_wins(self):
        if self._blue_wins is not None:
            return self._blue_wins

        self._blue_wins = self._calculate_blue_wins()

        return self._blue_wins

    @blue_wins.setter
    def blue_wins(self, value):
        self._blue_wins = value


class Series(CrestPage):
    def is_series_decided(self):
        return self.page_results['winner']['isDecided']

    def is_red_winner(self):
        if self.is_series_decided is False:
            return False

        if self.page_results['matchesWon']['redTeam'] > self.page_results['matchesWon']['blueTeam']:
            return True

        return False

    def is_blue_winner(self):
        if self.is_red_winner is True:
            return False

        return True


class Team(CrestPage):
    def get_name(self):
        return self.page_results['name']

    def get_ships_killed(self):
        return self.page_results['shipsKilled']

    def get_isk_killed(self):
        return self.page_results['iskKilled']

    def get_captain_id(self):
        return int(self.page_results['captain']['href'].split('/')[-2])

    def get_captain_name(self):
        return self.page_results['captain']['name']

    def get_pilots_fielded_count(self):
        return len(self.page_results['pilots'])