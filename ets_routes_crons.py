#!/usr/bin/env python

import logging
import ets_utils
import ets_tournaments
import ets_tournament_info
import ets_tournament_processes

from google.appengine.ext import deferred

from flask import Flask

app = Flask(__name__.split('.')[0])
app.secret_key = ets_utils.secret_key


@app.route('/crons/add_basic_tournament_info/<tourney_id>/')
def add_basic_tournament_info(tourney_id):
    ets_tournament_processes.process_update_basic_tournament(tourney_id)

    return ''


@app.route('/crons/update_tournaments/')
def update_tournaments():
    tournaments = ets_tournaments.get_running_tournaments()

    for tournament in tournaments:
        tournament_info = ets_tournament_info.get_tournament_info(tournament.key.id())

        for team_url in tournament_info.team_urls:
            deferred.defer(ets_tournament_processes.process_update_team,
                           tournament.key.id(),
                           team_url)

        deferred.defer(ets_tournament_processes.process_update_series,
                       tournament.key.id())

        deferred.defer(ets_tournament_processes.process_update_basic_tournament,
                       tournament.key.id())

    return ''